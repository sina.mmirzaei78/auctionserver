package com.sina.auctionProject.repository;

import com.sina.auctionProject.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface UserRepository extends PagingAndSortingRepository<User , Integer> {

    Optional<User> findByNationalCode(String nationalCode);
}
