package com.sina.auctionProject.repository;

import com.sina.auctionProject.model.User;
import com.sina.auctionProject.model.User_Auction;
import org.springframework.data.repository.PagingAndSortingRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface User_AuctionRepository extends PagingAndSortingRepository<User_Auction , Integer> {
    Optional<User_Auction> findByUser_IdAndAuction_Id(int userId , int auctionId);
    List<User_Auction> findAllByAuction_Id(int auctionId);
    Optional<User_Auction> findBySuggestedPrice(int price);
}
