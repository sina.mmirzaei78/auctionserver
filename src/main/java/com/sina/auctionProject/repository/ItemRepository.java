package com.sina.auctionProject.repository;

import com.sina.auctionProject.model.Item;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;

@Transactional
public interface ItemRepository extends PagingAndSortingRepository<Item , Integer> {

}
