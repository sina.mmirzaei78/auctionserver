package com.sina.auctionProject.repository;

import com.sina.auctionProject.model.Auction;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface AuctionRepository extends PagingAndSortingRepository<Auction , Integer> {
    List<Auction> findAllByIsSoldOutAndIsExpired(boolean soldOut , boolean expired);
    Optional<Auction> findByItem_Id(int itemId);
}
