package com.sina.auctionProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "auctionTbl")
public class Auction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private int basePrice;
    @NotNull
    private int increaseUnit;
    @NotNull
    private int finalPrice;
    @NotNull
    private long expireDate;
    @JsonIgnore
    private Boolean isSoldOut;
    @JsonIgnore
    private Boolean isExpired;

    @OneToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @JsonIgnore
    private Item item;

    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<User_Auction> user_auctions;


    //***********************************************//


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public int getIncreaseUnit() {
        return increaseUnit;
    }

    public void setIncreaseUnit(int increaseUnit) {
        this.increaseUnit = increaseUnit;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(long expireDate) {
        this.expireDate = expireDate;
    }

    public Boolean getSoldOut() {
        return isSoldOut;
    }

    public void setSoldOut(Boolean soldOut) {
        this.isSoldOut = soldOut;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<User_Auction> getUser_auctions() {
        return user_auctions;
    }

    public void setUser_auctions(List<User_Auction> user_auctions) {
        this.user_auctions = user_auctions;
    }
}
