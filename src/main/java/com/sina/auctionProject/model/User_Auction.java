package com.sina.auctionProject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@Table(name = "user_auctionTbl")
public class User_Auction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int suggestedPrice;
    private long suggestedDate;
    private Boolean isWinner;

    @ManyToOne
    @JoinColumn(name = "userId")
    @JsonIgnore
    private User user;

    @ManyToOne
    @JoinColumn(name = "auctionId")
    @JsonIgnore
    private Auction auction;

    //***********************************************//


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSuggestedPrice() {
        return suggestedPrice;
    }

    public void setSuggestedPrice(int suggestedPrice) {
        this.suggestedPrice = suggestedPrice;
    }

    public long getSuggestedDate() {
        return suggestedDate;
    }

    public void setSuggestedDate(long suggestedDate) {
        this.suggestedDate = suggestedDate;
    }

    public Boolean getWinner() {
        return isWinner;
    }

    public void setWinner(Boolean winner) {
        isWinner = winner;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }
}
