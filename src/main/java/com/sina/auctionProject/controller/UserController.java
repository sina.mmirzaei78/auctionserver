package com.sina.auctionProject.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sina.auctionProject.model.User;
import com.sina.auctionProject.repository.UserRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin(value = "*")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/user/signUp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String signUp(@RequestBody User user) {
        try {
            JsonObject jsonObject = new JsonObject();
            Optional<User> optionalUser = userRepository.findByNationalCode(user.getNationalCode());
            if (optionalUser.isPresent()) {
                jsonObject.addProperty("result", false);
                jsonObject.addProperty("message", "failed");
            } else {
                userRepository.save(user);
                jsonObject.addProperty("result", true);
                jsonObject.addProperty("message", "successful");
            }
            return new Gson().toJson(jsonObject);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/user/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String login(@RequestBody User user) {
        try {
            JsonObject jsonObject = new JsonObject();
            Optional<User> optionalUser = userRepository.findByNationalCode(user.getNationalCode());
            if (optionalUser.isPresent()) {
                jsonObject.addProperty("result", true);
                jsonObject.addProperty("message", "successful");
            } else {
                jsonObject.addProperty("result", false);
                jsonObject.addProperty("message", "failed");
            }
            return new Gson().toJson(jsonObject);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
