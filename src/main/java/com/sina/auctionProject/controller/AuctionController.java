package com.sina.auctionProject.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sina.auctionProject.model.Auction;
import com.sina.auctionProject.model.Item;
import com.sina.auctionProject.model.User;
import com.sina.auctionProject.model.User_Auction;
import com.sina.auctionProject.repository.AuctionRepository;
import com.sina.auctionProject.repository.ItemRepository;
import com.sina.auctionProject.repository.UserRepository;
import com.sina.auctionProject.repository.User_AuctionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(value = "*")
public class AuctionController {

    private final AuctionRepository auctionRepository;
    private final ItemRepository itemRepository;
    private final User_AuctionRepository user_auctionRepository;
    private final UserRepository userRepository;

    public AuctionController(AuctionRepository auctionRepository, ItemRepository itemRepository, User_AuctionRepository user_auctionRepository, UserRepository userRepository) {
        this.auctionRepository = auctionRepository;
        this.itemRepository = itemRepository;
        this.user_auctionRepository = user_auctionRepository;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/auction/add/{itemId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity addAuction(@RequestBody Auction auction, @PathVariable("itemId") int itemId) {
        try {
            Optional<Item> item = itemRepository.findById(itemId);
            if (item.isPresent()) {
                Optional<Auction> optionalAuction = auctionRepository.findByItem_Id(itemId);
                if (optionalAuction.isPresent() && optionalAuction.get().getSoldOut()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("result", false);
                    jsonObject.addProperty("message", "this item has soldOut");
                    return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                } else {
                    auction.setItem(item.get());
                    auction.setSoldOut(false);
                    auction.setExpired(false);
                    return new ResponseEntity<>(auctionRepository.save(auction), HttpStatus.OK);
                }
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/auction/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllAuction() {
        try {
            List<Auction> auctions = auctionRepository.findAllByIsSoldOutAndIsExpired(false, false);
            if (auctions.size() != 0) {
                return new ResponseEntity<>(auctions, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/auction/edit/{auctionId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity editAuction(@RequestBody Auction auction, @PathVariable("auctionId") int auctionId) {
        try {
            Optional<Auction> optionalAuction = auctionRepository.findById(auctionId);
            if (optionalAuction.isPresent()) {
                optionalAuction.get().setBasePrice(auction.getBasePrice());
                optionalAuction.get().setIncreaseUnit(auction.getIncreaseUnit());
                optionalAuction.get().setFinalPrice(auction.getFinalPrice());
                optionalAuction.get().setExpireDate(auction.getExpireDate());
                optionalAuction.get().setSoldOut(false);
                optionalAuction.get().setExpired(false);
                optionalAuction.get().setItem(optionalAuction.get().getItem());
                auctionRepository.save(optionalAuction.get());
                return new ResponseEntity<>(auctionRepository.save(optionalAuction.get()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/auction/participateAuction/{auctionId}/{userId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity participate(@RequestBody User_Auction user_auction, @PathVariable("auctionId") int auctionId, @PathVariable("userId") int userId) {
        try {
            Optional<Auction> optionalAuction = auctionRepository.findById(auctionId);
            Optional<User> optionalUser = userRepository.findById(userId);
            if (optionalAuction.isPresent()) {
                long time = System.currentTimeMillis();
                if (optionalAuction.get().getExpireDate() > time) {
                    if (!optionalAuction.get().getSoldOut()) {
                        if (optionalUser.isPresent()) {
                            List<User_Auction> user_auctions = user_auctionRepository.findAllByAuction_Id(auctionId);
                            Optional<User_Auction> optionalUser_auction = user_auctionRepository.findByUser_IdAndAuction_Id(userId, auctionId);
                            if (optionalUser_auction.isPresent()) {
                                int max = optionalAuction.get().getBasePrice();
                                for (int i = 0; i < user_auctions.size(); i++) {
                                    if (user_auctions.get(i).getSuggestedPrice() > max) {
                                        max = user_auctions.get(i).getSuggestedPrice();
                                    }
                                }
                                int n = ((optionalAuction.get().getFinalPrice() - optionalAuction.get().getBasePrice()) / optionalAuction.get().getIncreaseUnit());
                                boolean validPrice = false;
                                for (int i = 1; i <= n; i++) {
                                    if (user_auction.getSuggestedPrice() == optionalAuction.get().getBasePrice() || (user_auction.getSuggestedPrice() == (optionalAuction.get().getBasePrice()) + (i * optionalAuction.get().getIncreaseUnit())) || user_auction.getSuggestedPrice() == optionalAuction.get().getFinalPrice()) {
                                        validPrice = true;
                                        break;
                                    }
                                }

                                if (((user_auction.getSuggestedPrice() > max) || user_auction.getSuggestedPrice() == optionalAuction.get().getBasePrice()) && validPrice) {
                                    optionalUser_auction.get().setAuction(optionalAuction.get());
                                    optionalUser_auction.get().setUser(optionalUser.get());
                                    optionalUser_auction.get().setSuggestedPrice(user_auction.getSuggestedPrice());
                                    optionalUser_auction.get().setSuggestedDate(System.currentTimeMillis());
                                    if (user_auction.getSuggestedPrice() == optionalAuction.get().getFinalPrice()) {
                                        //finish the auction with final price
                                        optionalAuction.get().setBasePrice(optionalAuction.get().getBasePrice());
                                        optionalAuction.get().setIncreaseUnit(optionalAuction.get().getIncreaseUnit());
                                        optionalAuction.get().setFinalPrice(optionalAuction.get().getFinalPrice());
                                        optionalAuction.get().setExpireDate(System.currentTimeMillis());
                                        optionalAuction.get().setSoldOut(true);
                                        optionalAuction.get().setExpired(true);
                                        optionalAuction.get().setItem(optionalAuction.get().getItem());
                                        auctionRepository.save(optionalAuction.get());
                                        optionalUser_auction.get().setWinner(true);
                                        return new ResponseEntity<>(user_auctionRepository.save(optionalUser_auction.get()), HttpStatus.OK);
                                    } else {
                                        optionalUser_auction.get().setWinner(false);
                                        return new ResponseEntity<>(user_auctionRepository.save(optionalUser_auction.get()), HttpStatus.OK);
                                    }
                                } else {
                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("result", false);
                                    jsonObject.addProperty("message", "suggested price is not acceptable");
                                    return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                                }
                            } else {
                                int max = optionalAuction.get().getBasePrice();
                                for (int i = 0; i < user_auctions.size(); i++) {
                                    if (user_auctions.get(i).getSuggestedPrice() > max) {
                                        max = user_auctions.get(i).getSuggestedPrice();
                                    }
                                }
                                int n = ((optionalAuction.get().getFinalPrice() - optionalAuction.get().getBasePrice()) / optionalAuction.get().getIncreaseUnit());
                                boolean validPrice = false;
                                for (int i = 1; i <= n; i++) {
                                    if ((user_auction.getSuggestedPrice() == optionalAuction.get().getBasePrice()) || (user_auction.getSuggestedPrice() == (optionalAuction.get().getBasePrice()) + (i * optionalAuction.get().getIncreaseUnit())) || user_auction.getSuggestedPrice() == optionalAuction.get().getFinalPrice()) {
                                        validPrice = true;
                                        break;
                                    }
                                }

                                if (((user_auction.getSuggestedPrice() > max) || user_auction.getSuggestedPrice() == optionalAuction.get().getBasePrice()) && validPrice) {
                                    user_auction.setAuction(optionalAuction.get());
                                    user_auction.setUser(optionalUser.get());
                                    user_auction.setSuggestedPrice(user_auction.getSuggestedPrice());
                                    user_auction.setSuggestedDate(System.currentTimeMillis());
                                    if (user_auction.getSuggestedPrice() == optionalAuction.get().getFinalPrice()) {
                                        //finish the auction with final price
                                        optionalAuction.get().setBasePrice(optionalAuction.get().getBasePrice());
                                        optionalAuction.get().setIncreaseUnit(optionalAuction.get().getIncreaseUnit());
                                        optionalAuction.get().setFinalPrice(optionalAuction.get().getFinalPrice());
                                        optionalAuction.get().setExpireDate(System.currentTimeMillis());
                                        optionalAuction.get().setSoldOut(true);
                                        optionalAuction.get().setExpired(true);
                                        optionalAuction.get().setItem(optionalAuction.get().getItem());
                                        auctionRepository.save(optionalAuction.get());
                                        user_auction.setWinner(true);
                                        return new ResponseEntity<>(user_auctionRepository.save(user_auction), HttpStatus.OK);
                                    } else {
                                        user_auction.setWinner(false);
                                        return new ResponseEntity<>(user_auctionRepository.save(user_auction), HttpStatus.OK);
                                    }
                                } else {
                                    JsonObject jsonObject = new JsonObject();
                                    jsonObject.addProperty("result", false);
                                    jsonObject.addProperty("message", "suggested price is not acceptable");
                                    return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                                }

                            }
                        } else {
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("result", false);
                            jsonObject.addProperty("message", "this user is not existed");
                            return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.NOT_FOUND);
                        }


                    } else {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("result", false);
                        jsonObject.addProperty("message", "this auction has soldOut");
                        return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.OK);
                    }

                } else {

                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("result", false);
                    jsonObject.addProperty("message", "this auction has expired");
                    return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.OK);
                }
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("result", false);
                jsonObject.addProperty("message", "this auction is not available");
                return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.NOT_FOUND);
            }

        } catch (
                Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/auction/getExpiredAuctionWinner/{auctionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getExpiredAuctionWinner(@PathVariable("auctionId") int auctionId) {
        try {
            Optional<Auction> optionalAuction = auctionRepository.findById(auctionId);
            if (optionalAuction.isPresent()) {
                if (optionalAuction.get().getExpireDate() < System.currentTimeMillis()) {
                    if (!optionalAuction.get().getSoldOut()) {
                        //set the winner
                        List<User_Auction> user_auctions = user_auctionRepository.findAllByAuction_Id(auctionId);
                        if (user_auctions.size() > 0) {
                            int max = optionalAuction.get().getBasePrice();
                            for (User_Auction userAuction : user_auctions) {
                                if (userAuction.getSuggestedPrice() > max) {
                                    max = userAuction.getSuggestedPrice();
                                }
                            }
                            Optional<User_Auction> optionalUserAuction = user_auctionRepository.findBySuggestedPrice(max);
                            optionalUserAuction.get().setSuggestedDate(optionalUserAuction.get().getSuggestedDate());
                            optionalUserAuction.get().setWinner(true);
                            optionalUserAuction.get().setAuction(optionalAuction.get());
                            //find the user
                            Optional<User> optionalUser = userRepository.findById(optionalUserAuction.get().getUser().getId());
                            optionalUserAuction.get().setUser(optionalUser.get());
                            optionalUserAuction.get().setSuggestedPrice(optionalUserAuction.get().getSuggestedPrice());
                            user_auctionRepository.save(optionalUserAuction.get());
                            //update auction status
                            optionalAuction.get().setBasePrice(optionalAuction.get().getBasePrice());
                            optionalAuction.get().setIncreaseUnit(optionalAuction.get().getIncreaseUnit());
                            optionalAuction.get().setFinalPrice(optionalAuction.get().getFinalPrice());
                            optionalAuction.get().setExpireDate(optionalAuction.get().getExpireDate());
                            optionalAuction.get().setSoldOut(true);
                            optionalAuction.get().setExpired(true);
                            auctionRepository.save(optionalAuction.get());
                            return new ResponseEntity<>(optionalUserAuction.get(), HttpStatus.OK);

                        } else {
                            //no winner about this auction
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("result", false);
                            jsonObject.addProperty("message", "this auction has expired without winner");
                            //delete an auction without winner!
                            auctionRepository.delete(optionalAuction.get());
                            return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                        }
                    } else {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("result", false);
                        jsonObject.addProperty("message", "this auction has soldOut");
                        return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                    }
                } else {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("result", false);
                    jsonObject.addProperty("message", "this auction has not expired");
                    return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
                }
            } else {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("result", false);
                jsonObject.addProperty("message", "this auction is not available");
                return new ResponseEntity<>(new Gson().toJson(jsonObject), HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}