package com.sina.auctionProject.controller;

import com.sina.auctionProject.model.Item;
import com.sina.auctionProject.repository.ItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Optional;


@RestController
@CrossOrigin(value = "*")
public class ItemController {

    private final ItemRepository itemRepository;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @RequestMapping(value = "/item/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity addItem(@Valid @RequestBody(required = true) Item item) {
        try {
            return new ResponseEntity<>(itemRepository.save(item), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/item/edit/{itemId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity editItem(@Valid @RequestBody(required = true) Item item , @PathVariable("itemId") int itemId) {
        try {
            Optional<Item> findItem = itemRepository.findById(itemId);
            if (findItem.isPresent()){
                findItem.get().setDescription(item.getDescription());
                return new ResponseEntity<>(itemRepository.save(findItem.get()), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/item/delete/{itemId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteItem(@PathVariable("itemId") int itemId) {
        try {
            Optional<Item> item = itemRepository.findById(itemId);
            if (item.isPresent()){
                itemRepository.delete(item.get());
                return new ResponseEntity<>( HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
